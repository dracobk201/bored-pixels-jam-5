﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveBehaviour : MonoBehaviour
{
    private Animator anim;
    [SerializeField]
    private IntReference ObjectiveMaxImpacts;
    [SerializeField]
    private IntReference ObjectiveActualImpacts;
    [SerializeField]
    private GameEvent ObjectiveDamaged;
    [SerializeField]
    private GameEvent ObjectiveDestroyed;

    private void Start()
    {
        anim = GetComponent<Animator>();
        ObjectiveActualImpacts.Value = ObjectiveMaxImpacts.Value;
    }

    public void ObjectiveApplyingDamaged()
    {
        ObjectiveActualImpacts.Value--;
        if (ObjectiveActualImpacts.Value<= 0)
        {
            ObjectiveActualImpacts.Value = 0;
            ObjectiveDestroyed.Raise();
            anim.SetTrigger(Global.AnimationDestroy);
        }
    }
}
