﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour
{
    [SerializeField]
    private BoolReference GamePaused;

    #region Bullets
    [Header("Bullet Variables")]
    private List<GameObject> Bullets;
    [SerializeField]
    private IntReference BulletsPooled;
    [SerializeField]
    private GameObject BulletPrefab;
    [SerializeField]
    private Transform BulletMagazine;
    [SerializeField]
    private Transform bulletPosition;
    [SerializeField]
    private FloatReference MinFireFrequency;
    [SerializeField]
    private FloatReference MaxFireFrequency;
    #endregion

    #region Weapon
    [SerializeField]
    private GameEvent EnemyShot;
    #endregion

    private float counter;

    private void Start()
    {
        InstantiateBullets();
    }

    private void Update()
    {
        counter -= Time.deltaTime;
        if (counter <= 0)
            ShootBullet();
    }

    public void ShootBullet()
    {
        if (GamePaused.Value)
            return;

        for (int i = 0; i < Bullets.Count; i++)
        {
            if (!Bullets[i].activeInHierarchy)
            {
                Bullets[i].transform.position = bulletPosition.position;
                Bullets[i].transform.rotation = bulletPosition.rotation;
                Bullets[i].SetActive(true);
                EnemyShot.Raise();
                counter = Random.Range(MinFireFrequency.Value, MaxFireFrequency.Value);
                break;
            }
        }
    }

    private void InstantiateBullets()
    {
        Bullets = new List<GameObject>();
        for (int i = 0; i < BulletsPooled.Value; i++)
        {
            GameObject bang = Instantiate(BulletPrefab) as GameObject;
            bang.GetComponent<Transform>().SetParent(BulletMagazine.transform);
            bang.SetActive(false);
            Bullets.Add(bang);
        }
    }
}
