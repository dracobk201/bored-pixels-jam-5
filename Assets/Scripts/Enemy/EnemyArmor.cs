﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArmor : MonoBehaviour
{
    private Animator anim;
    [SerializeField]
    private IntReference EnemyMaxImpacts;
    [SerializeField]
    private IntReference EnemiesOnScreen;
    [SerializeField]
    private GameEvent EnemyDestroyed;

    private int remainingImpacts;

    private void Start()
    {
        remainingImpacts = EnemyMaxImpacts.Value;
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals(Global.PlayerBulletTag))
        {
            DamageReceived();
        }

        if (other.transform.tag.Equals(Global.DestroyerTag))
        {
            DamageReceived(true);
        }
    }

    private void DamageReceived(bool isDestroyer=false)
    {
        remainingImpacts--;

        if (remainingImpacts <= 0 || isDestroyer)
        {
            EnemiesOnScreen.Value--;
            EnemyDestroyed.Raise();
            anim.SetTrigger(Global.AnimationDestroy);
        }
    }

    public void DestroyMe()
    {
        gameObject.SetActive(false);
    }
}
