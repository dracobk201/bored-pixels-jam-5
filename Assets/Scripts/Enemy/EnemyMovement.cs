﻿using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField]
    private BoolReference GamePaused;
    [SerializeField]
    private FloatReference MoveSpeed;
    private int facing;

    public int Facing
    {
        get
        {
            return facing;
        }
        set
        {
            facing = value;
            if (facing == -1)
                transform.rotation = new Quaternion(0, 180f, 0, 0);
            else
                transform.rotation = new Quaternion(0, 0, 0, 0);
        }
    }

    private void Awake()
    {
        Physics2D.IgnoreLayerCollision(8, 9, true);
    }

    private void FixedUpdate()
    {
        Moving();
    }

    private void Moving()
    {
        if (GamePaused.Value)
            return;

        Vector3 oldPosition = transform.position;
        float newDirection = facing * MoveSpeed.Value * Time.deltaTime;
        transform.position = new Vector3(oldPosition.x + newDirection, oldPosition.y, 0);
    }
}