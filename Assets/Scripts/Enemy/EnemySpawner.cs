﻿using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private BoolReference GamePaused;

    #region Initial Elements
    [Header("Initial Elements")]
    [SerializeField]
    private LevelRuntimeSet Levels;
    [SerializeField]
    private Transform enemiesHangar;
    [SerializeField]
    private GameObject flyingEnemyPrefab;
    [SerializeField]
    private GameObject groundEnemyPrefab;
    [SerializeField]
    private IntReference EnemiesPooled;
    [SerializeField]
    private FloatReference AdditionalTimeToWait;
    [SerializeField]
    private FloatReference XOriginPosition;
    [SerializeField]
    private FloatReference GroundHeightSpawn;
    [SerializeField]
    private FloatReference MinHeightSpawn;
    [SerializeField]
    private FloatReference MaxHeightSpawn;
    #endregion

    #region Updateable Elements
    [Header("Updateable Elements")]
    [SerializeField]
    private IntReference LastLevelBeated;
    [SerializeField]
    private IntReference ActualWave;
    [SerializeField]
    private FloatReference RemainingTime;
    [SerializeField]
    private IntReference actualEnemyOnScreen;
    #endregion

    #region Game Events
    [Header("Game Events")]
    [SerializeField]
    private GameEvent FlyingEnemySpawned;
    [SerializeField]
    private GameEvent GroundEnemySpawned;
    [SerializeField]
    private GameEvent WaveFinished;
    [SerializeField]
    private GameEvent LevelFinished;
    #endregion

    #region Internal Variables
    private List<GameObject> flyingEnemies;
    private List<GameObject> groundEnemies;

    private float nextSpawnTime;
    private float actualSpawnTime;
    private float maxEnemyOnScreen;
    #endregion

    private void Start()
    {
        InstantiateEnemiesPool();
        actualEnemyOnScreen.Value = 0;
        ActualWave.Value = 0;
        LastLevelBeated.Value = 0;
        InitLevel();
    }

    public void InitLevel()
    {
        RemainingTime.Value = Levels.Items[LastLevelBeated.Value].Waves[ActualWave.Value].Time.Value;
        nextSpawnTime = Levels.Items[LastLevelBeated.Value].Waves[ActualWave.Value].EnemyFrequency.Value;
        maxEnemyOnScreen = Levels.Items[LastLevelBeated.Value].Waves[ActualWave.Value].MaxEnemyInScreen.Value;
        actualSpawnTime = nextSpawnTime;
    }

    private void Update()
    {
        if (GamePaused.Value)
            return;

        CheckSpawningEnemyTime();
        CheckWaveTime();
    }

    private void CheckWaveTime()
    {
        RemainingTime.Value -= Time.deltaTime;

        if (RemainingTime.Value <= 0)
        {
            WaveFinished.Raise();
            ActualWave.Value++;
            if (ActualWave.Value > Levels.Items[LastLevelBeated.Value].Waves.Count)
            {
                LastLevelBeated.Value++;
                ActualWave.Value = 0;
            }
            InitLevel();
        }
    }

    private void CheckSpawningEnemyTime()
    {
        actualSpawnTime -= Time.deltaTime;
        if (actualSpawnTime <= 0)
        {
            if (actualEnemyOnScreen.Value < maxEnemyOnScreen)
            {
                if (Random.value > 0.5f)
                    SpawnFlyingEnemy();
                else
                    SpawnGroundEnemy();
            }
            else
            {
                actualSpawnTime += AdditionalTimeToWait.Value;
            }
        }
    }

    private void SpawnFlyingEnemy()
    {
        Transform targetLocation = GeneratePosition(Random.value, true);

        for (int i = 0; i < flyingEnemies.Count; i++)
        {
            if (!flyingEnemies[i].activeInHierarchy)
            {
                flyingEnemies[i].transform.position = targetLocation.position;
                if (Mathf.Round(targetLocation.rotation.y) == 1f || Mathf.Round(targetLocation.rotation.y) == 180f)
                    flyingEnemies[i].GetComponent<EnemyMovement>().Facing = -1;
                else
                    flyingEnemies[i].GetComponent<EnemyMovement>().Facing = 1;
                flyingEnemies[i].SetActive(true);
                actualEnemyOnScreen.Value++;
                FlyingEnemySpawned.Raise();
                actualSpawnTime = Random.Range(nextSpawnTime - Random.value, nextSpawnTime + Random.value);
                break;
            }
        }
    }

    private void SpawnGroundEnemy()
    {
        Transform targetLocation = GeneratePosition(Random.value, false);

        for (int i = 0; i < groundEnemies.Count; i++)
        {
            if (!groundEnemies[i].activeInHierarchy)
            {
                groundEnemies[i].transform.position = targetLocation.position;
                if (Mathf.Round(targetLocation.rotation.y) == 1f || Mathf.Round(targetLocation.rotation.y) == 180f)
                    groundEnemies[i].GetComponent<EnemyMovement>().Facing = -1;
                else
                    groundEnemies[i].GetComponent<EnemyMovement>().Facing = 1;
                groundEnemies[i].SetActive(true);
                actualEnemyOnScreen.Value++;
                GroundEnemySpawned.Raise();
                actualSpawnTime = Random.Range(nextSpawnTime - Random.value, nextSpawnTime + Random.value);
                break;
            }
        }
    }

    private void InstantiateEnemiesPool()
    {
        flyingEnemies = new List<GameObject>();
        groundEnemies = new List<GameObject>();
        for (int i = 0; i < EnemiesPooled.Value; i++)
        {
            GameObject flying = Instantiate(flyingEnemyPrefab) as GameObject;
            flying.name = "Drone " + i;
            flying.GetComponent<Transform>().SetParent(enemiesHangar);
            flying.SetActive(false);
            flyingEnemies.Add(flying);

            GameObject ground = Instantiate(groundEnemyPrefab) as GameObject;
            ground.name = "Tank " + i;
            ground.GetComponent<Transform>().SetParent(enemiesHangar);
            ground.SetActive(false);
            groundEnemies.Add(ground);
        }
    }

    private Transform GeneratePosition(float flipProbability, bool isFlying)
    {
        Transform target = transform;
        Vector2 position = Vector2.zero;
        Quaternion rotation = Quaternion.identity;

        float newXPosition = XOriginPosition.Value;
        float newYPosition = GroundHeightSpawn.Value;
        float newYRotation = 180;

        if (isFlying)
        {
            float tempA = MinHeightSpawn.Value * 2;
            int a = (int) tempA;
            float tempB = MaxHeightSpawn.Value * 2;
            int b = (int) tempB;
            newYPosition = Random.Range(a, b) / 2;
        }

        if (flipProbability <= 0.5f)
        {
            newXPosition *= -1;
            newYRotation = 0;
        }

        position.x = newXPosition;
        position.y = newYPosition;
        rotation.y = newYRotation;

        target.position = position;
        target.rotation = rotation;

        return target;
    }
}
