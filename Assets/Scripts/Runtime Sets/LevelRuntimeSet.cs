﻿
using UnityEngine;

[CreateAssetMenu(menuName = "Runtime Set/Levels")]
public class LevelRuntimeSet : RuntimeSet<Level>
{ }
