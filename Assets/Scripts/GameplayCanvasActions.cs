﻿using UnityEngine;
using UnityEngine.UI;

public class GameplayCanvasActions : MonoBehaviour
{
    [Header("Wave & Level Variables")]
    [SerializeField]
    private IntReference ActualWave;
    [SerializeField]
    private IntReference ActualLevel;
    [SerializeField]
    private FloatReference RemainingTime;
    [SerializeField]
    private Text waveLevelLabel;
    [SerializeField]
    private Text timeText;

    [Header("Player Variables")]
    [SerializeField]
    private IntReference MaxArmor;
    [SerializeField]
    private IntReference ActualArmor;
    [SerializeField]
    private Image armorGauge;

    [Header("Objective Variables")]
    [SerializeField]
    private IntReference ObjectiveMaxArmor;
    [SerializeField]
    private IntReference ObjectiveActualArmor;
    [SerializeField]
    private Image objectiveArmorGauge;

    private void Start()
    {
        UpdateWaveLevel();
    }

    private void Update()
    {
        UpdateLevelTimer();
    }

    private void UpdateLevelTimer()
    {
        int minutes = Mathf.FloorToInt(RemainingTime.Value / 60f);
        int seconds = Mathf.RoundToInt(RemainingTime.Value % 60f);

        string formatedSeconds = seconds.ToString();

        if (seconds == 60)
        {
            seconds = 0;
            minutes += 1;
        }

        timeText.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }

    public void UpdateWaveLevel()
    {
        int actualLevel = ActualLevel.Value + 1;
        int actualWave = ActualWave.Value + 1;
        waveLevelLabel.text = actualLevel + " - " + actualWave;
    }

    public void UpdateArmorGauge()
    {
        armorGauge.fillAmount = (float) ActualArmor.Value / MaxArmor.Value;
    }

    public void UpdateObjectiveArmorGauge()
    {
        objectiveArmorGauge.fillAmount = (float)ObjectiveActualArmor.Value / ObjectiveMaxArmor.Value;
    }

}
