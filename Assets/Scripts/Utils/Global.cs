﻿

public static class Global
{
    #region Tags
    public const string DestroyerTag = "Destroyer";
    public const string GroundTag = "Ground";
    public const string ObjectiveTag = "Objective";
    public const string PlayerTag = "Player";
    public const string PlayerBulletTag = "PlayerBullet";
    public const string EnemyTag = "Enemy";
    public const string EnemyBulletTag = "EnemyBullet";
    #endregion

    #region Axis
    public const string HorizontalAxis = "Horizontal";
    public const string VerticalAxis = "Vertical";
    public const string JumpAxis = "Jump";
    public const string StartAxis = "Cancel";
    public const string FireAxis = "Fire1";
    #endregion

    #region Scene Names
    public const string MainMenuScene = "Main Menu";
    public const string FirstLevelScene = "Level";
    #endregion

    #region Animations
    public const string AnimationRight = "Moving";
    public const string AnimationUp = "FacingUp";
    public const string AnimationJump = "Jump";
    public const string AnimationDestroy = "Destroyed";
    #endregion

}
