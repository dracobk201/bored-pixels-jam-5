﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : MonoBehaviour
{
    [SerializeField]
    private IntReference PlayerMaxImpacts;
    [SerializeField]
    private IntReference PlayerRemainingImpacts;
    [SerializeField]
    private GameEvent PlayerDestroyed;

    private void Start()
    {
        PlayerRemainingImpacts.Value = PlayerMaxImpacts.Value;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals(Global.EnemyBulletTag))
        {
            DamageReceived();
        }
    }

    private void DamageReceived()
    {
        PlayerRemainingImpacts.Value--;

        if (PlayerRemainingImpacts.Value <= 0)
        {
            transform.position = Vector2.zero;
            PlayerDestroyed.Raise();
            gameObject.SetActive(false);
        }
    }
}
