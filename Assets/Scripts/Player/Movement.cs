﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private BoolReference GamePaused;
    [SerializeField]
    private FloatReference HorizontalAxis;
    [SerializeField]
    private FloatReference VerticalAxis;
    [SerializeField]
    private FloatReference MoveSpeed;
    [SerializeField]
    private FloatReference JumpSpeed;
    [SerializeField]
    private IntReference PlayerFacing;
    [SerializeField]
    private FloatReference MaxJumpHigh;
    private Rigidbody2D playerRigidbody2D;
    private bool isGrounded;
    

    private void Awake()
    {
        playerRigidbody2D = GetComponent<Rigidbody2D>();
        PlayerFacing.Value = 1;
    }

    private void FixedUpdate()
    {
        Facing();
        Move();
    }

    private void Facing()
    {
        if (GamePaused.Value)
            return;

        if (VerticalAxis.Value > 0)
        {
            PlayerFacing.Value = 0;
        }   
        else
        {
            if (HorizontalAxis.Value != 0)
            {
                if (HorizontalAxis.Value < 0)
                    PlayerFacing.Value = -1;
                else
                    PlayerFacing.Value = 1;
            }
        }
    }

    private void Move()
    {
        if (GamePaused.Value)
            return;
        if (HorizontalAxis.Value != 0)
        {
            Vector2 oldPosition = playerRigidbody2D.position;
            float newDirection = HorizontalAxis.Value * MoveSpeed.Value * Time.deltaTime;
            playerRigidbody2D.position = new Vector2(oldPosition.x + newDirection, oldPosition.y);
        }
    }

    public void Jump()
    {
        if (GamePaused.Value)
            return;

        if (isGrounded)
        {
            isGrounded = false;

            float t = playerRigidbody2D.velocity.y / MaxJumpHigh.Value;
            float lerp = 0;

            if (t >= 0f)
                lerp = Mathf.Lerp(MaxJumpHigh.Value, 0f, t);
            else if (t < 0f)
                lerp = Mathf.Lerp(MaxJumpHigh.Value, 0f, Mathf.Abs(t));

            Vector2 force = new Vector2(0f, lerp * JumpSpeed.Value);
            playerRigidbody2D.AddForce(Vector2.up * force * Time.fixedDeltaTime, ForceMode2D.Impulse);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag.Equals(Global.GroundTag))
        {
            isGrounded = true;
        }
    }
}
