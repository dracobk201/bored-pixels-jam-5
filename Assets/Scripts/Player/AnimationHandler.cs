﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    [SerializeField]
    private IntReference PlayerFacing;
    [SerializeField]
    private FloatReference HorizontalAxis;
    [SerializeField]
    private FloatReference VerticalAxis;
    private Animator animator;
    private int lastFacing;
    private bool isJumping;
    private bool isGrounded;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        CheckAnimations();
    }

    private void CheckAnimations()
    {
        if (HorizontalAxis.Value != 0)
        {
            animator.SetBool("Walking", true);
        }
        else
            animator.SetBool("Walking", false);
        if (VerticalAxis.Value > 0 || PlayerFacing.Value==0)
        {
            animator.SetBool("Vertical", true);
        }
        else
            animator.SetBool("Vertical", false);
        //if (isJumping)
        if (!isGrounded)

        {
            animator.SetBool("isJumping", true);
            //isJumping = false;
        }
        else
        {
            animator.SetBool("isJumping", false);
        }


            
        
        if (HorizontalAxis.Value < 0)
        {
            transform.GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (HorizontalAxis.Value > 0)
        {
            transform.GetComponent<SpriteRenderer>().flipX = false;
        }

    }

    public void Jumping()
    {
        isJumping = true;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag.Equals(Global.GroundTag))
        {
            isGrounded = true;
        }
    }
    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag.Equals(Global.GroundTag))
        {
            isGrounded = false;
        }
    }

}
