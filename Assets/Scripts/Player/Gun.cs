﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField]
    private BoolReference GamePaused;

    #region Bullets
    [Header("Bullet Variables")]
    private List<GameObject> Bullets;
    [SerializeField]
    private IntReference BulletsPooled;
    [SerializeField]
    private GameObject BulletPrefab;
    [SerializeField]
    private Transform BulletMagazine;
    [SerializeField]
    private Transform bulletRightPosition;
    [SerializeField]
    private Transform bulletLeftPosition;
    [SerializeField]
    private Transform bulletUpPosition;
    #endregion

    #region Weapon
    [SerializeField]
    private GameEvent PlayerShot;
    [SerializeField]
    private IntReference PlayerFacing;
    #endregion

    private void Start()
    {
        InstantiateBullets();
    }

    public void ShootBullet()
    {
        if (GamePaused.Value)
            return;

        Vector2 initialPosition = Vector2.zero;
        Quaternion initialRotation = Quaternion.identity;

        switch (PlayerFacing.Value)
        {
            case -1:
                initialPosition = bulletLeftPosition.position;
                initialRotation = bulletLeftPosition.rotation;
                break;
            case 0:
                initialPosition = bulletUpPosition.position;
                initialRotation = bulletUpPosition.rotation;
                break;
            case 1:
            default:
                initialPosition = bulletRightPosition.position;
                initialRotation = bulletRightPosition.rotation;
                break;
        }

        for (int i = 0; i < Bullets.Count; i++)
        {
            if (!Bullets[i].activeInHierarchy)
            {
                Bullets[i].transform.position = initialPosition;
                Bullets[i].transform.rotation = initialRotation;
                Bullets[i].SetActive(true);
                PlayerShot.Raise();
                break;
            }
        }
    }

    private void InstantiateBullets()
    {
        Bullets = new List<GameObject>();
        for (int i = 0; i < BulletsPooled.Value; i++)
        {
            GameObject bang = Instantiate(BulletPrefab) as GameObject;
            bang.GetComponent<Transform>().SetParent(BulletMagazine.transform);
            bang.SetActive(false);
            Bullets.Add(bang);
        }
    }
}
